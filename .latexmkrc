@default_files = (
    'academic_cv.tex',
    'professional_cv.tex'
);
$out_dir = 'out';
#$pdflatex = "xelatex %O %S";
$pdf_mode = 1;
$dvi_mode = 0;
$postscript_mode = 0;
